export default function (sprite) {
    const hiddenBox = document.createElement('div')

    hiddenBox.style.cssText = 'height: 0; width: 0; position: absolute; pointer-events: none;'
    hiddenBox.insertAdjacentHTML('afterbegin', sprite)

    document.body.append(hiddenBox)
}
